import {app, BrowserWindow, ipcMain, screen, shell} from 'electron';
import * as path from 'path';
import * as url from 'url';
import * as settings from 'electron-settings';
import logger from "./src/app/core/utils/log";

import {IPCElectronStore, IPCElectronStoreActions} from "./src/app/core/ipc/IPCElectronStore";
import {IPCElectronActionsActions} from "./src/app/core/ipc/IPCElectronActions";

const MODULE = "ElectronMainProcess"

let win: BrowserWindow = null;
const args = process.argv.slice(1),
  serve = args.some(val => val === '--serve');

function createWindow(): BrowserWindow {

  const electronScreen = screen;
  const size = electronScreen.getPrimaryDisplay().workAreaSize;

  // Create the browser window.
  win = new BrowserWindow({
    //x: 0,
    //y: 0,
    //width: size.width,
    //height: size.height,
    minWidth: 800,
    minHeight: 600,
    // frame: false,
    // titleBarStyle: 'hidden',
    webPreferences: {
      nodeIntegration: true,
      nodeIntegrationInWorker: true,
      allowRunningInsecureContent: serve,
      enableRemoteModule: false,
    },
    icon: `${app.getAppPath()}/src/assets/icons/favicon.ico`
  });

  if (serve) {
    win.webContents.openDevTools();
    require('electron-reload')(__dirname, {
      electron: require(`${__dirname}/node_modules/electron`)
    });
    win.loadURL('http://localhost:4200');
  } else {
    win.loadURL(url.format({
      pathname: path.join(__dirname, 'dist/index.html'),
      protocol: 'file:',
      slashes: true
    }));
  }

  win.setMenu(null)

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store window
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  return win;
}

try {
  app.allowRendererProcessReuse = true;

  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  // Added 400 ms to fix the black background issue while using transparent window. More detais at https://github.com/electron/electron/issues/15947
  app.on('ready', () => {
    setTimeout(createWindow, 400)
    bridgeCompanionStart()
  });

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit();
    }
    bridgeCompanionStop()
  });

  app.on('activate', () => {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createWindow();
      bridgeCompanionStart();
    }
  });

} catch (e) {
  // Catch Error
  // throw e;
}

/*
 * Bridge Companion Management
 */

function bridgeCompanionStart() {
  // TODO check if bridge is already running
  let companion_path = `${serve ? app.getAppPath() : process.resourcesPath}/itbridge/itbridge${process.platform === 'darwin' ? "" : ".exe"}`

  // try to start bridge
  let bridgeProcess = require('child_process').spawn(
    require.resolve(companion_path),
    {detached: false, shell: false}
    // ['--port', '5400'], // pass to process.argv into child
    // (err, data) => {}
  )
  bridgeProcess.on('data', (data) => {
    logger.debug("Bridge companion logged: " + data, {module: MODULE})
  })
  bridgeProcess.on('exit', (code, sig) => {
    logger.info("Bridge companion ended with code " + code, {module: MODULE})
  })
  bridgeProcess.on('error', (error) => {
    logger.error("Cannot start the bridge companion", {module: MODULE, error})
  })
}

function bridgeCompanionStop() {

}

/*
 * IPC Events
 */

/*
 * -> Store
 */

ipcMain.handle(IPCElectronStore.getMainActionId(IPCElectronStoreActions.GET_VALUE), (event, key) => {
  let value = settings.getSync(key)
  logger.debug(`Retrieving Store[${key}]=${value}`, {module: MODULE})
  return value == undefined ? IPCElectronStore.getConfigurationDefaultValue(key) : value;
});

ipcMain.handle(IPCElectronStore.getMainActionId(IPCElectronStoreActions.SET_VALUE), (event, ...args) => {
  let key = args[0]
  let payload = args[1]
  logger.debug(`Setting Store[${key}]=${JSON.stringify(payload)}`, {module: MODULE})
  return settings.setSync(key, payload);
});

ipcMain.handle(IPCElectronActionsActions.OPEN_EXTERNAL_URL, (event, ...args) => {
  let url = args[0]
  shell.openExternal(url).then(_ => {})
})
