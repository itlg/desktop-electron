/**
 * Custom angular webpack configuration
 */

const ThreadsPlugin = require('threads-plugin')

module.exports = (config, options) => {
  config.target = 'electron-renderer'

  if (options.customWebpackConfig.target) {
    config.target = options.customWebpackConfig.target
  } else if (options.fileReplacements) {
    for (let fileReplacement of options.fileReplacements) {
      if (fileReplacement.replace !== 'src/environments/environment.ts') {
        continue
      }

      let fileReplacementParts = fileReplacement['with'].split('.')
      if (['dev', 'prod', 'test', 'electron-renderer'].indexOf(fileReplacementParts[1]) < 0) {
        config.target = fileReplacementParts[1]
      }
      break
    }
  }

  config.plugins = [
    ...config.plugins,
    new ThreadsPlugin({
      target: 'electron-node-worker',
      plugins: config.plugins,
      globalObject: 'self'
    })
  ]

  return config
}
