set DIR="./src/assets/protos/"
set OUT_DIR="./src/app/core/services/grpc/stubs"

:protoc -I=%DIR% itbridge.proto --js_out=import_style=commonjs,binary:%OUT_DIR% --grpc-web_out=import_style=typescript,mode=grpcwebtext:%OUT_DIR%

npx grpc_tools_node_protoc -I=%DIR% itbridge.proto --js_out=import_style=commonjs,binary:%OUT_DIR% --grpc_out=generate_package_definition:%OUT_DIR%

protoc -I=%DIR% itbridge.proto --plugin=protoc-gen-ts=./node_modules/.bin/protoc-gen-ts --ts_out=%OUT_DIR%
