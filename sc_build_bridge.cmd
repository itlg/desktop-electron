cd ../itbridge-win
devenv itbridge-win.sln /build Release

cd ../desktop-electron
rm -r itbridge
mkdir itbridge
cp -r ../itbridge-win/bin/Release/*.exe ./itbridge
cp -r ../itbridge-win/bin/Release/*.dll ./itbridge
