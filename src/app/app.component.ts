import {Component} from '@angular/core';
import {ElectronService} from './core/services/electron/electron.service';
import {TranslateService} from '@ngx-translate/core';
import {AppConfig} from '../environments/environment';
import logger from "./core/utils/log";
import {AnalyticsService} from "./core/services/analytics/analytics.service";
import {version} from '../../package.json';

const MODULE = "AppComponent"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private electronService: ElectronService,
    private translate: TranslateService,
    private analyticsService: AnalyticsService
  ) {
    this.translate.setDefaultLang('en');

    // log the app opened
    analyticsService.logStartedApplication(version)

    logger.debug(`AppConfig: ${JSON.stringify(AppConfig)}`, {module: MODULE});

    if (electronService.isElectron) {
      logger.debug(`${JSON.stringify(process.env)}`, {module: MODULE});
      logger.debug('Run in electron', {module: MODULE});
      logger.debug(`Electron ipcRenderer: ${this.electronService.ipcRenderer}`, {module: MODULE});
      logger.debug(`NodeJS childProcess: ${this.electronService.childProcess}`, {module: MODULE});
    } else {
      logger.debug('Run in browser', {module: MODULE});
    }
  }

}
