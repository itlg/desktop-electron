import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  sidenavOpened: boolean

  constructor(public router: Router) {
    this.router.events.subscribe((e) => {
      if(this.sidenavOpened) this.sidenavOpened = false
    })
  }

  ngOnInit(): void {
  }

  /*
   * Listeners
   */

  onToolbarMenuClick() {
    this.sidenavOpened = !this.sidenavOpened
  }

}
