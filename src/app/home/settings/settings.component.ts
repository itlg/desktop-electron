import {Component, OnInit} from '@angular/core';
import {StoreService} from "../../core/services/store/store.service";
import {IPCElectronStoreKeys} from "../../core/ipc/IPCElectronStore";

const MODULE = "SettingsComponent"

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  storeKeys = IPCElectronStoreKeys

  radioOvwLyrics: boolean = false
  radioLogging: boolean = false
  radioAnalytics: boolean = false
  radioUpdates: boolean = false

  constructor(public store: StoreService) {

  }

  async ngOnInit() {
    this.radioOvwLyrics = await this.store.get(IPCElectronStoreKeys.CONFIGURATION_OVERWRITE_LYRICS)
    this.radioLogging = await this.store.get(IPCElectronStoreKeys.CONFIGURATION_LOGGING)
    this.radioAnalytics = await this.store.get(IPCElectronStoreKeys.CONFIGURATION_ANALYTICS)
    this.radioUpdates = await this.store.get(IPCElectronStoreKeys.CONFIGURATION_AUTO_CHECK_UPDATES)
  }

  checkChange(key: IPCElectronStoreKeys) {
    switch (key) {
      case IPCElectronStoreKeys.CONFIGURATION_ANALYTICS:
        this.store.set(key, this.radioAnalytics).then((res) => {
        })
        break
      case IPCElectronStoreKeys.CONFIGURATION_AUTO_CHECK_UPDATES:
        this.store.set(key, this.radioUpdates).then((res) => {
        })
        break
      case IPCElectronStoreKeys.CONFIGURATION_OVERWRITE_LYRICS:
        this.store.set(key, this.radioOvwLyrics).then((res) => {
        })
        break
      case IPCElectronStoreKeys.CONFIGURATION_LOGGING:
        this.store.set(key, this.radioLogging).then((res) => {
        })
        break
    }
  }

}
