import {CollectionViewer, DataSource} from "@angular/cdk/collections";
import {BehaviorSubject, Observable} from "rxjs";
import {Track} from "../../core/lib/itbridge/stubs/itbridge_pb";

// https://blog.angular-university.io/angular-material-data-table/

export enum TrackStatus {
  SEARCHING = "SEARCHING",
  ADDING_LYRICS = "ADDING_LYRICS",
  LYRICS_FOUND = "LYRICS_FOUND",
  LYRICS_NOT_FOUND = "LYRICS_NOT_FOUND",
  ERROR = "ERROR",
  ERROR_ITUNES = "ERROR_ITUNES",
  ERROR_WEB = "ERROR_WEB",
  LYRICS_FOUND_AND_ADDED = "LYRICS_FOUND_AND_ADDED",
  SKIPPED = "SKIPPED",
  CANCELED = "CANCELED",
  ALREADY_HAS_LYRICS = "ALREADY_HAS_LYRICS"
}

/**
 * This class represents a track in the table, that is a standard Track with associated a status
 */
class TableTrack {
  private track: Track
  private trackStatus: TrackStatus
  private trackStatusParameters: Array<String>

  constructor(t: Track, s: TrackStatus, p?: Array<String>) {
    this.track = t
    this.trackStatus = s
    this.trackStatusParameters = p
  }

  getTrack() {
    return this.track
  }

  getStatus(): TrackStatus {
    return this.trackStatus
  }

  getStatusString() {
    switch (this.trackStatus.valueOf()) {
      case TrackStatus.ERROR:
        return "Error"
      case TrackStatus.SEARCHING:
        return "Searching for lyrics"
      case TrackStatus.LYRICS_FOUND:
        return "Found lyrics"
      case TrackStatus.LYRICS_FOUND_AND_ADDED:
        return "Lyrics added"
      case TrackStatus.ERROR_ITUNES:
        return "iTunes error"
      case TrackStatus.SKIPPED:
        return "Skipped"
      case TrackStatus.ERROR_WEB:
        return "Web Error"
      case TrackStatus.ADDING_LYRICS:
        return "Adding Lyrics to iTunes"
      case TrackStatus.CANCELED:
        return "Canceled by user"
      case TrackStatus.LYRICS_NOT_FOUND:
        return "Lyrics not found"
      case TrackStatus.ALREADY_HAS_LYRICS:
        return "Already has lyrics"
    }
    return this.trackStatus
  }

  getStatusColor() {
    switch (this.trackStatus) {
      case TrackStatus.ERROR:
        return "#d32f2f" // red600
      case TrackStatus.SEARCHING:
        return "#fdd835" // yellow600
      case TrackStatus.LYRICS_FOUND:
        return "#9ccc65" // lightgreen400
      case TrackStatus.LYRICS_FOUND_AND_ADDED:
        return "#4caf50"
      case TrackStatus.ERROR_ITUNES:
        return "#d81b60"
      case TrackStatus.SKIPPED:
        return "#ffb300"
      case TrackStatus.ERROR_WEB:
        return "#b71c1c"
      case TrackStatus.ADDING_LYRICS:
        return "#fdd835" // yellow600
      case TrackStatus.CANCELED:
        return "#607d8b"
      case TrackStatus.ALREADY_HAS_LYRICS:
        return "#9ccc65"
      case TrackStatus.LYRICS_NOT_FOUND:
        return "#f4511e"
    }
    return "#ffffff"
  }

  getStatusBlinking() {
    switch (this.trackStatus) {
      case TrackStatus.ERROR:
      case TrackStatus.LYRICS_FOUND_AND_ADDED:
      case TrackStatus.ERROR_ITUNES:
      case TrackStatus.SKIPPED:
      case TrackStatus.ERROR_WEB:
      case TrackStatus.CANCELED:
      case TrackStatus.ALREADY_HAS_LYRICS:
      case TrackStatus.LYRICS_NOT_FOUND:
        return false
      case TrackStatus.SEARCHING:
      case TrackStatus.LYRICS_FOUND:
      case TrackStatus.ADDING_LYRICS:
        return true
    }
    return false
  }

  setStatus(s: TrackStatus, p?: Array<String>) {
    this.trackStatus = s
    this.trackStatusParameters = p
  }
}

/**
 * This datasource handles the tracks (TableTracks) displayed in the main table
 */
export class ProcessedTracksDataSource implements DataSource<TableTrack> {

  private tracks: Array<TableTrack> = []
  private trackSubject = new BehaviorSubject<TableTrack[]>([]);

  constructor() {
  }

  connect(collectionViewer: CollectionViewer): Observable<TableTrack[]> {
    return this.trackSubject.asObservable()
  }

  disconnect(collectionViewer: CollectionViewer): void {
    return this.trackSubject.complete()
  }

  loadTracks() {
    this.trackSubject.next(this.tracks);
  }

  /**
   * Get the total number of tracks
   */
  length() {
    return this.tracks.length
  }

  /**
   * Add track to the list
   * @param t
   * @return The index in the list for updating track information
   */
  addTrack(t: Track): number {
    this.tracks.unshift(new TableTrack(t, TrackStatus.SEARCHING))
    this.loadTracks()
    return this.tracks.length - 1
  }

  /**
   * Delete all tracks from the data source
   */
  clearTracks() {
    this.tracks = []
    this.loadTracks()
  }

  /**
   * Update the status for the track at given index
   * @param trackIndex
   * @param trackStatus
   * @param trackStatusParameters
   */
  updateStatusForTrack(trackIndex: number, trackStatus: TrackStatus, trackStatusParameters?: Array<String>) {
    if (trackIndex < 0 || trackIndex >= this.tracks.length) return
    this.tracks[this.tracks.length - trackIndex - 1].setStatus(trackStatus, trackStatusParameters)
    this.loadTracks()
  }
}
