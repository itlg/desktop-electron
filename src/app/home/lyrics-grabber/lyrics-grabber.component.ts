import {Component, OnInit} from '@angular/core';
import {ProcessedTracksDataSource, TrackStatus} from "./processedTracks.datasource";
import {TrackProcessorTrackAction, TracksProcessor} from "../../core/lib/trackProcessor/trackProcessor";
import {Track} from "../../core/lib/itbridge/stubs/itbridge_pb";
import logger from "../../core/utils/log";
import {AnalyticsService} from "../../core/services/analytics/analytics.service";
import {version} from "../../../../package.json"
import {StoreService} from "../../core/services/store/store.service";
import {IPCElectronStoreKeys} from "../../core/ipc/IPCElectronStore";
import {Updates} from "../../core/lib/updates/updates";
import {IPCElectronActionsActions} from "../../core/ipc/IPCElectronActions";
import {ElectronService} from "../../core/services/electron/electron.service";

const MODULE = "LyricsGrabberComponent"

@Component({
  selector: 'app-lyrics-grabber',
  templateUrl: './lyrics-grabber.component.html',
  styleUrls: ['./lyrics-grabber.component.css']
})
export class LyricsGrabberComponent implements OnInit {

  processedTracksDataSource: ProcessedTracksDataSource;
  processedTracksTableDisplayedColumns = ["no", "title", "album", "artist", "status"]
  mProcessing = false
  mIsStopping = false
  mUpdatesAvailable = false
  mUpdatesUrl: string = ""

  private mTrackProcessor: TracksProcessor

  constructor(private analyticsService: AnalyticsService, private storeService: StoreService, private electronService: ElectronService) {
    this.processedTracksDataSource = new ProcessedTracksDataSource()
    this.mTrackProcessor = new TracksProcessor(
      undefined, // {getTrackStream: iTBridgeGRPC.getInstance().getSelectedTracks} as TrackProcessorStreamHandler,
      {
        onProcessTrack: (t: Track) => {
          // logger.info(`onProcessTrack: started ${t.getTitle()}`, {module: MODULE})
          this.analyticsService.logDownloadLyricsStarted()
        },
        onEndProcessTrack: (t: Track, s: TrackStatus) => {
          // logger.info(`onEndProcessTrack: for ${t.getTitle()}, result=${s}`, {module: MODULE})
          if (s == TrackStatus.LYRICS_FOUND_AND_ADDED)
            this.analyticsService.logDownloadLyricsSuccess()
          else
            this.analyticsService.logDownloadLyricsError()
          this.analyticsService.logDownloadLyricsEndStatus(s)
        }
      } as TrackProcessorTrackAction,
      this.processedTracksDataSource
    )

    // check for updates
    Updates.checkForUpdates().then(res => {
      this.mUpdatesAvailable = res
    })
  }

  ngOnInit(): void {
  }


  /*
   * Listeners
   */

  async onGrabClick() {
    this.analyticsService.logGrabPressed()

    // if track processor is already processing stop it
    if (this.mProcessing) {
      this.mIsStopping = true
      await this.mTrackProcessor.stop()
      this.mProcessing = false
      this.mIsStopping = false
      return
    }

    // get overwrite config
    let overWriteLyrics = false
    await this.storeService.get(IPCElectronStoreKeys.CONFIGURATION_OVERWRITE_LYRICS).then(res => {
      overWriteLyrics = res
    })

    // otherwise start
    this.mProcessing = true
    this.mTrackProcessor.start(!overWriteLyrics).then(res => {
      logger.debug("TrackProcessor Terminated", {module: MODULE})
      this.mProcessing = false
    }).catch(error => {
      logger.error("TrackProcessor Error", {module: MODULE, error})
      this.mProcessing = false
    })
  }

  /*
   * UI
   */

  getStatusBarPhrase() {
    if (this.mIsStopping) return "Stopping current running jobs..."
    if (this.mProcessing) return "Processing tracks..."
    return "Ready"
  }

  getAppVersion() {
    return version
  }

  /*
   * Utils
   */

  openLink(url: string, e: Event) {
    e.preventDefault()
    this.electronService.ipcRenderer.invoke(IPCElectronActionsActions.OPEN_EXTERNAL_URL, url).then()
  }
}
