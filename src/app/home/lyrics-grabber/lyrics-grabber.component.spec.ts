import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LyricsGrabberComponent } from './lyrics-grabber.component';

describe('LyricsGrabberComponent', () => {
  let component: LyricsGrabberComponent;
  let fixture: ComponentFixture<LyricsGrabberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LyricsGrabberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LyricsGrabberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
