import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HomeRoutingModule} from './home-routing.module';

import {SharedModule} from '../shared/shared.module';
import {LyricsGrabberComponent} from './lyrics-grabber/lyrics-grabber.component';
import {SettingsComponent} from './settings/settings.component';
import {HomeComponent} from "./home.component";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatListModule} from "@angular/material/list";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import {AboutComponent} from './about/about.component';

@NgModule({
  declarations: [HomeComponent, LyricsGrabberComponent, SettingsComponent, AboutComponent],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatSlideToggleModule,
  ]
})
export class HomeModule {

}
