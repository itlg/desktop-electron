import {Component, OnInit} from '@angular/core';
import {version} from '../../../../package.json';
import {ElectronService} from "../../core/services/electron/electron.service";
import {IPCElectronActionsActions} from "../../core/ipc/IPCElectronActions";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(private electronService: ElectronService) {
  }

  ngOnInit(): void {
  }

  getAppVersion() {
    return version
  }

  openLink(url: string, e: Event) {
    e.preventDefault()
    this.electronService.ipcRenderer.invoke(IPCElectronActionsActions.OPEN_EXTERNAL_URL, url).then()
  }
}
