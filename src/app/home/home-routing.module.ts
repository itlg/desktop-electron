import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LyricsGrabberComponent} from "./lyrics-grabber/lyrics-grabber.component";
import {SettingsComponent} from "./settings/settings.component";
import {HomeComponent} from "./home.component";
import {AboutComponent} from "./about/about.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        redirectTo: 'lyrics-grabber',
      },
      {
        path: 'lyrics-grabber',
        component: LyricsGrabberComponent,
      },
      {
        path: 'settings',
        component: SettingsComponent,
      },
      {
        path: 'about',
        component: AboutComponent,
      },
    ]
  }

];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
