import * as winston from "winston";
import * as dailyRotateFile from "winston-daily-rotate-file";
import {ElectronNode} from "./electronNode";

/*
 * Utils
 */

const getFormat = () => winston.format.combine(
  // winston.format.label({label: 'MY-SILLY-APP'}),
  winston.format.timestamp(),
  winston.format.splat(),
  winston.format.metadata({fillExcept: ['message', 'level', 'timestamp']}),
  winston.format.colorize(),
  winston.format.printf(info => {
    let out = `${info.timestamp} ${info.level} [${info.metadata.module}]: ${info.message}`;
    if (info.metadata.error) {
      out = out + ' ' + info.metadata.error;
      if (info.metadata.error.stack) {
        out = out + ' ' + info.metadata.error.stack;
      }
    }
    return out;
  }))


/*
 * Core
 */

const logger = winston.createLogger({
  level: 'debug',
  format: getFormat(),
  // defaultMeta: {service: 'user-service'},
  transports: getTransports(),
});

function getTransports() {
  // for retrieving app logs path we need the electron.app object, only reachable from node main process
  if (!ElectronNode.isRenderer() && !ElectronNode.isWorker()) {
    let logPath = console.log(require('electron').app.getPath('logs'))
    return [
      //
      // - Write all logs with level `error` and below to `error.loPas
      new dailyRotateFile({filename: `${logPath}/logs/error.log`, level: 'error', maxSize: '20m'}),
      new dailyRotateFile({filename: `${logPath}/logs/combined.log`, maxSize: '20m'}),
    ]
  }
  return []
}

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: getFormat(),
  }));
}

export default logger

