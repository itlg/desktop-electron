import {ipcRenderer, webFrame, remote, net} from 'electron';
import * as childProcess from 'child_process';
import * as fs from 'fs';

export class ElectronNode {
  private static instance: ElectronNode;

  ipcRenderer: typeof ipcRenderer;
  webFrame: typeof webFrame;
  remote: typeof remote;
  childProcess: typeof childProcess;
  fs: typeof fs;
  net: typeof net;

  constructor() {
    // Conditional imports
    if (this.isElectron && window != undefined) {
      this.ipcRenderer = window.require('electron').ipcRenderer;
      this.webFrame = window.require('electron').webFrame;
      this.remote = window.require('electron').remote;
      this.net = window.require('electron').net;

      this.childProcess = window.require('child_process');
      this.fs = window.require('fs');
    }
  }

  get isElectron(): boolean {
    if (window == undefined) return false
    return !!(window && window.process && window.process.type);
  }

  public static isRenderer() {
    // running in a web browser
    if (typeof process === 'undefined') return true

    // node-integration is disabled
    if (!process) return true

    // We're in node.js somehow
    if (!process.type) return false

    return process.type === 'renderer'
  }

  public static isNode() {
    return typeof process === 'object' && typeof require === 'function'
  }

  public static isWeb() {
    return typeof window === 'object';
  }

  public static isWorker() {
    return typeof window === 'undefined';
  }

  public static isShell() {
    return !this.isWeb() && !this.isNode() && !this.isWorker()
  }

  public static getInstance(): ElectronNode {
    if (!ElectronNode.instance) {
      ElectronNode.instance = new ElectronNode();
    }

    return ElectronNode.instance;
  }
}
