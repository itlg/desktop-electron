import axios from 'axios'
import logger from "../../utils/log";

const API_URL = "https://www.google-analytics.com/collect";

const QUERY_TAG_VERSION = "v";
const QUERY_TAG_PROPERTY_ID = "tid";
const QUERY_TAG_ANONYMOUS_CLIENT_ID = "cid";
const QUERY_TAG_HIT_TYPE = "t";

const QUERY_TYPE_EVENT_VALUE = "event";
const QUERY_TAG_EVENT_CATEGORY = "ec";
const QUERY_TAG_EVENT_ACTION = "ea";
const QUERY_TAG_EVENT_LABEL = "el";
const QUERY_TAG_EVENT_VALUE = "ev";

const QUERY_TYPE_EXCEPTION_VALUE = "exception";
const QUERY_TAG_EXCEPTION_DESCRIPTION = "exd";
const QUERY_TAG_EXCEPTION_FATAL = "exf";

const QUERY_TYPE_PAGE_VALUE = "pageview";
const QUERY_TAG_PAGE_HOSTNAME = "dh";
const QUERY_TAG_PAGE_PAGE = "dp";
const QUERY_TAG_PAGE_TITLE = "dt";

const MODULE = "GAnalytics"

/**
 * This module implements GAnalytics Logging
 */
export default class GAnalytics {

  private readonly mUUID: string = ""
  private readonly mUAID: string = ""

  constructor(propertyId: string, uuid: string) {
    this.mUAID = propertyId
    this.mUUID = uuid
  }

  private getBaseQueryDict() {
    let d = {}
    d[QUERY_TAG_VERSION] = 1
    d[QUERY_TAG_PROPERTY_ID] = this.mUAID
    d[QUERY_TAG_ANONYMOUS_CLIENT_ID] = this.mUUID
    return d
  }

  /*
   * Main Loggers
   */

  logEvent(eventCategory: string, eventAction: string, eventLabel: string, eventValue: Number) {
    const d = this.getBaseQueryDict()
    d[QUERY_TAG_HIT_TYPE] = QUERY_TYPE_EVENT_VALUE;
    d[QUERY_TAG_EVENT_CATEGORY] = eventCategory;
    d[QUERY_TAG_EVENT_ACTION] = eventAction;
    d[QUERY_TAG_EVENT_LABEL] = eventLabel;
    d[QUERY_TAG_EVENT_VALUE] = eventValue;

    axios.get(API_URL, {params: d}).then(res => {
      logger.debug("logEvent: done", {module: MODULE})
    }).catch(err => {
      logger.debug(`logEvent: error (${JSON.stringify(err)})`, {module: MODULE})
    })
  }

  logException(exceptionDescription: string, exceptionFatal: Boolean) {
    const d = this.getBaseQueryDict()
    d[QUERY_TAG_HIT_TYPE] = QUERY_TYPE_EXCEPTION_VALUE;
    d[QUERY_TAG_EXCEPTION_DESCRIPTION] = exceptionDescription;
    d[QUERY_TAG_EXCEPTION_FATAL] = exceptionFatal;

    axios.get(API_URL, {params: d}).then(res => {
      logger.debug("logException: done", {module: MODULE})
    }).catch(err => {
      logger.debug(`logException: error (${JSON.stringify(err)})`, {module: MODULE})
    })
  }

  logPageView(hostname: string, page: string, title: string) {
    const d = this.getBaseQueryDict()
    d[QUERY_TAG_HIT_TYPE] = QUERY_TYPE_PAGE_VALUE;
    d[QUERY_TAG_PAGE_HOSTNAME] = hostname;
    d[QUERY_TAG_PAGE_PAGE] = page;
    d[QUERY_TAG_PAGE_TITLE] = title;

    axios.get(API_URL, {params: d}).then(res => {
      logger.debug("logPageView: done", {module: MODULE})
    }).catch(err => {
      logger.debug(`logPageView: error (${JSON.stringify(err)})`, {module: MODULE})
    })
  }

}
