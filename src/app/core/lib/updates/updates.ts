import {version, versionCode} from '../../../../../package.json'
import request from 'request-promise-native'

const UPDATES_JSON = "https://gitlab.com/itlg/releases/-/raw/master/win/latest.json"

export class Updates {

  static checkForUpdates(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      request({uri: UPDATES_JSON, json: true}).then(res => {
        if (res["beta"]["versionCode"] > versionCode || res["stable"]["versionCode"] > versionCode)
          resolve(true)
        else
          resolve(false)
      }).catch(err => reject(err))
    })
  }

}

