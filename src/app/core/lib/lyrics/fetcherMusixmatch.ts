import {LyricsFetcher} from "./lyricsFetcher";
import request from 'request-promise-native';
import cheerio from 'cheerio';

import logger from "../../utils/log";

const MODULE = "FetcherMusixmatch"

export class FetcherMusixmatch implements LyricsFetcher {
  setTimeout(timeout: Number) {

  }

  getLyrics(title: string, album: string, artist: string): Promise<string> {
    return new Promise<string>(async (resolve, reject) => {
      let lyrics = ""
      let lyricsLink = ""

      logger.debug(`${MODULE}: Fetching ${FetcherMusixmatch.prepareSearchLink(title, album, artist)}`, {module: MODULE})
      // first request search lyrics
      await request(this.prepareRequestOptions(FetcherMusixmatch.prepareSearchLink(title, album, artist))).then($ => {
        // get first link to lyrics
        lyricsLink = $('a', 'h2.media-card-title').attr("href")
        logger.debug(`${MODULE}: Call#1: Lyrics Link ${lyricsLink}`, {module: MODULE})
      }).catch(err => {
        reject(err)
      })

      // second request get lyrics
      console.log(`${MODULE}: Fetching ${FetcherMusixmatch.prepareLyricsLink(lyricsLink)}`)
      await request(this.prepareRequestOptions(FetcherMusixmatch.prepareLyricsLink(lyricsLink))).then($ => {
        // find lyrics blocks
        let blocks = $('.mxm-lyrics__content')
        logger.debug(`${MODULE}: Call#2: Found ${blocks.length} blocks`, {module: MODULE})

        // unify them
        blocks.each(function (i, e) {
          lyrics += (i > 0 ? "\n" : "") + $(this).text()
          // logger.debug(`${MODULE}: Call#2: block #${i} = ${$(this).text()}`, {module: MODULE})
        })

        logger.debug(`${MODULE}: Call#2: #2 Lyrics len ${lyrics.length}`, {module: MODULE})
      }).catch(err => {
        reject(err)
      })

      resolve(lyrics)
    })
  }

  /*
   * Utils
   */

  private static prepareSearchLink(title: string, album: string, artist: string): string {
    return `https://www.musixmatch.com/search/${encodeURIComponent(title)}-${encodeURIComponent(artist)}/tracks`
  }

  private static prepareLyricsLink(lyricsLink: string): string {
    return `https://www.musixmatch.com${lyricsLink}`
  }

  private prepareRequestOptions(link): any {
    return {
      uri: link,
      transform: function (body) {
        return cheerio.load(body);
      }
    };
  }

}
