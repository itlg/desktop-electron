export interface LyricsFetcher {
  setTimeout(timeout: Number)
  getLyrics(title: string, album: string, artist: string): Promise<string>
}
