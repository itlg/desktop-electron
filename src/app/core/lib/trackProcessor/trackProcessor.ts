import * as grpc from "@grpc/grpc-js";
import {ClientReadableStream} from "@grpc/grpc-js";
import {Pool, spawn, Worker} from "threads/dist";
import {ProcessedTracksDataSource, TrackStatus} from "../../../home/lyrics-grabber/processedTracks.datasource";
import {iTBridgeGRPC} from "../itbridge/iTBridgeGRPC";
import {Track, TracksList, TracksListStream} from "../itbridge/stubs/itbridge_pb";
import logger from "../../utils/log";

const MODULE = "TracksProcessor";

export interface TrackProcessorStreamHandler {
  getTrackStream(onData: (t: TracksListStream) => void, onError: (e: grpc.ServiceError) => void, onStatus: (s: any) => void, onEnd: () => void): ClientReadableStream<TracksListStream>
}

/**
 * The list of tracks on which the processor must work
 */
export enum TrackProcessorTrackListType {
  SELECTED_TRACKS
}

export interface TrackProcessorTrackList {
  type: TrackProcessorTrackListType
}

/**
 * The action to perform to each track
 */
export interface TrackProcessorTrackAction {
  onProcessTrack: (t: Track) => void
  onEndProcessTrack: (t: Track, status: TrackStatus) => void
}

export class TracksProcessor {
  private mTrackList: TrackProcessorTrackList
  private mTrackAction: TrackProcessorTrackAction

  private readonly mDataSource: ProcessedTracksDataSource
  private mIsProcessing = false
  private mPool = undefined

  constructor(trackList: TrackProcessorTrackList, trackAction: TrackProcessorTrackAction, dataSource?: ProcessedTracksDataSource) {
    this.mTrackList = trackList
    this.mTrackAction = trackAction
    this.mDataSource = dataSource
  }

  async start(skipAlreadyHasLyrics: boolean = true) {
    return new Promise<boolean>(async (resolve, reject) => {
      if (this.mIsProcessing) return reject(`${MODULE} is already processing!`)
      logger.debug("TrackStreamProcessor: start", {module: MODULE})
      this.mIsProcessing = true
      // stop the previous pool
      if (this.mPool !== undefined) await this.mPool.terminate(true)
      // create the pool
      this.mPool = Pool(() => spawn(new Worker("./trackProcessor.worker")), {size: 2, name: "TrackStreamProcessorPool"})
      // clear tracks on the data source
      if (this.mDataSource) this.mDataSource.clearTracks()
      // retrieve tracks
      iTBridgeGRPC.getInstance().getSelectedTracks()
        .then(async res => {
          await this.onTracksReady(res, skipAlreadyHasLyrics)
          resolve(true)
        })
        .catch(err => {
          this.mIsProcessing = false
          logger.error(`TrackStreamProcessor: no track!`, {module: MODULE, error: err})
          reject(err)
        })
    })
  }

  async stop() {
    this.mIsProcessing = false
    await this.mPool.terminate(true)
  }

  async onTracksReady(list: TracksList, skipAlreadyHasLyrics: boolean = true) {
    list.getTracksList().forEach((t, i) => {
      let trackIndex = -1
      this.mPool.queue(async processTrack => {
        // onProcessTrack listener
        if (this.mTrackAction.onProcessTrack != undefined) this.mTrackAction.onProcessTrack(t)
        // add track to db
        if (this.mDataSource) trackIndex = this.mDataSource.addTrack(t)
        var hasLyrics = false
        // check if track has lyrics
        if (skipAlreadyHasLyrics) {
          await iTBridgeGRPC.getInstance().getTrackHasLyrics(t.getId().getIdSource(), t.getId().getIdDatabase(), t.getId().getIdPlaylist(), t.getId().getIdTrack()).then(resHasLyrics => {
            if (resHasLyrics) {
              this.mDataSource.updateStatusForTrack(trackIndex, TrackStatus.ALREADY_HAS_LYRICS)
              hasLyrics = true
            }
          })
        }
        if (hasLyrics) return

        // retrieve lyrics
        let lyrics = await processTrack(t.getTitle(), t.getArtist(), t.getAlbum())

        // if lyrics are found, send them to iTunes
        if (lyrics !== "") {
          this.mDataSource.updateStatusForTrack(trackIndex, TrackStatus.ADDING_LYRICS)
          await iTBridgeGRPC.getInstance().setTrackLyrics(t.getId().getIdSource(), t.getId().getIdDatabase(), t.getId().getIdPlaylist(), t.getId().getIdTrack(), lyrics)
            .then(res => {
              this.mDataSource.updateStatusForTrack(trackIndex, TrackStatus.LYRICS_FOUND_AND_ADDED)
              if (this.mTrackAction.onEndProcessTrack != undefined) this.mTrackAction.onEndProcessTrack(t, TrackStatus.LYRICS_FOUND_AND_ADDED)
            }).catch(err => {
              this.mDataSource.updateStatusForTrack(trackIndex, TrackStatus.ERROR_ITUNES)
              if (this.mTrackAction.onEndProcessTrack != undefined) this.mTrackAction.onEndProcessTrack(t, TrackStatus.ERROR_ITUNES)
            })
          // set lyrics
        } else {
          this.mDataSource.updateStatusForTrack(trackIndex, TrackStatus.LYRICS_NOT_FOUND)
          if (this.mTrackAction.onEndProcessTrack != undefined) this.mTrackAction.onEndProcessTrack(t, TrackStatus.LYRICS_NOT_FOUND)
        }
        logger.debug("Returned lyrics of size " + lyrics.length, {module: MODULE})
      }).then(res => {
      }).catch(error => {
        logger.debug("Task queue error, maybe interrupted worker?", {module: MODULE, error})
        this.mDataSource.updateStatusForTrack(trackIndex, TrackStatus.CANCELED)
      })
    })
    // wait for pool to complete all the tasks
    await this.mPool.completed()
    await this.mPool.terminate()

    this.mIsProcessing = false
  }
}
