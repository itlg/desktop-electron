import {expose} from "threads/worker";
import {FetcherMusixmatch} from "../lyrics/fetcherMusixmatch";
import logger from "../../utils/log";

const MODULE = "TrackProcessor.WebWorker"

expose(async function fetch(trackTitle, trackAlbum, trackArtist) {
  logger.debug("fetch: processing " + trackTitle, {module: MODULE})

  let lyrics = ""

  let fetcher = new FetcherMusixmatch()
  await fetcher.getLyrics(trackTitle, trackAlbum, trackArtist)
    .then(res => {
      lyrics = res
    })
    .catch(err => {
      logger.error("fetch: Error " + err, {module: MODULE})
    })

  logger.debug("fetch: Got lyrics of size " + lyrics.length, {module: MODULE})

  return lyrics
})
