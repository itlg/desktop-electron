import * as grpc from '@grpc/grpc-js';
import {ClientReadableStream} from '@grpc/grpc-js';
import {PackageDefinition} from "@grpc/proto-loader";

import * as protoDefinition from './stubs/itbridge_grpc_pb'
import {iTBridgeServiceClient} from './stubs/itbridge_grpc_pb'
import {
  PayloadEmpty,
  PayloadOperationResult,
  iTObjectId,
  TrackIdAndLyrics,
  TracksList,
  TracksListStream
} from "./stubs/itbridge_pb";
import logger from "../../utils/log";

const MODULE = "iTBridgeGRPC"

export class iTBridgeGRPC {
  private static instance: iTBridgeGRPC;

  private readonly client: iTBridgeServiceClient;

  private constructor() {
    const packageDefinition = grpc.loadPackageDefinition(protoDefinition as unknown as PackageDefinition)
    // @ts-ignore
    const iTBridgeServiceClient = packageDefinition.itbridge.iTBridgeService as unknown as iTBridgeServiceClient

    // @ts-ignore
    this.client = new iTBridgeServiceClient('localhost:50051', grpc.credentials.createInsecure(), {})
  }

  public static getInstance(): iTBridgeGRPC {
    if (!iTBridgeGRPC.instance) {
      iTBridgeGRPC.instance = new iTBridgeGRPC();
    }

    return iTBridgeGRPC.instance;
  }

  /*
   * Exported
   */

  hello() {
    this.client.hello(new PayloadEmpty(), (err, res) => {
      console.log("GrpcServiceTs:sayHello:res:" + res)
      console.log("GrpcServiceTs:sayHello:err:" + err)
    })
  }

  streamSelectedTracks(onData: (t: TracksListStream) => void, onError: (e: grpc.ServiceError) => void, onStatus: (s: any) => void, onEnd: () => void): ClientReadableStream<TracksListStream> {
    // console.log("getSelectedTracksStream: client is " + this.client)
    const streamedTracks = this.client.tracksStreamSelected(new PayloadEmpty())
    streamedTracks.on('data', onData)
    streamedTracks.on('err', onError)
    streamedTracks.on('status', onStatus)
    streamedTracks.on('end', onEnd)
    return streamedTracks
  }

  getSelectedTracks(): Promise<TracksList> {
    logger.debug(`called getSelectedTracks -- client is undefined? ${this.client === undefined || this.client === null}`, {module: MODULE})
    return new Promise<TracksList>((resolve, reject) => {
      this.client.tracksGetSelected(new PayloadEmpty(), (err, res) => {
        if (err) reject(err); else resolve(res)
      })
    })
  }

  setTrackLyrics(sourceId: number, databaseId: number, playlistId: number, trackId: number, lyrics: string): Promise<PayloadOperationResult> {
    return new Promise<PayloadOperationResult>((resolve, reject) => {
      const trackIds = new iTObjectId()
      trackIds.setIdSource(sourceId)
      trackIds.setIdDatabase(databaseId)
      trackIds.setIdPlaylist(playlistId)
      trackIds.setIdTrack(trackId)
      const trackIdAndLyrics = new TrackIdAndLyrics()
      trackIdAndLyrics.setId(trackIds)
      trackIdAndLyrics.setLyrics(lyrics)

      this.client.trackSetLyrics(trackIdAndLyrics, (err, res) => {
        if (err) reject(err); else resolve(res)
      })
    })
  }

  getTrackHasLyrics(sourceId: number, databaseId: number, playlistId: number, trackId: number): Promise<Boolean> {
    return new Promise<Boolean>(((resolve, reject) => {
      const trackIds = new iTObjectId()
      trackIds.setIdSource(sourceId)
      trackIds.setIdDatabase(databaseId)
      trackIds.setIdPlaylist(playlistId)
      trackIds.setIdTrack(trackId)
      this.client.trackGetHasLyrics(trackIds, (err, res) => {
        if (err) reject(err); else resolve(res.getContent())
      })
    }))
  }
}
