// package: itbridge
// file: itbridge.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as itbridge_pb from "./itbridge_pb";

interface IiTBridgeServiceService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    hello: IiTBridgeServiceService_IHello;
    tracksStreamSelected: IiTBridgeServiceService_ITracksStreamSelected;
    tracksGetSelected: IiTBridgeServiceService_ITracksGetSelected;
    trackSetLyrics: IiTBridgeServiceService_ITrackSetLyrics;
    trackGetHasLyrics: IiTBridgeServiceService_ITrackGetHasLyrics;
    playlistsGet: IiTBridgeServiceService_IPlaylistsGet;
    playlistGetTracks: IiTBridgeServiceService_IPlaylistGetTracks;
}

interface IiTBridgeServiceService_IHello extends grpc.MethodDefinition<itbridge_pb.PayloadEmpty, itbridge_pb.HelloReply> {
    path: string; // "/itbridge.iTBridgeService/Hello"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<itbridge_pb.PayloadEmpty>;
    requestDeserialize: grpc.deserialize<itbridge_pb.PayloadEmpty>;
    responseSerialize: grpc.serialize<itbridge_pb.HelloReply>;
    responseDeserialize: grpc.deserialize<itbridge_pb.HelloReply>;
}
interface IiTBridgeServiceService_ITracksStreamSelected extends grpc.MethodDefinition<itbridge_pb.PayloadEmpty, itbridge_pb.TracksListStream> {
    path: string; // "/itbridge.iTBridgeService/TracksStreamSelected"
    requestStream: false;
    responseStream: true;
    requestSerialize: grpc.serialize<itbridge_pb.PayloadEmpty>;
    requestDeserialize: grpc.deserialize<itbridge_pb.PayloadEmpty>;
    responseSerialize: grpc.serialize<itbridge_pb.TracksListStream>;
    responseDeserialize: grpc.deserialize<itbridge_pb.TracksListStream>;
}
interface IiTBridgeServiceService_ITracksGetSelected extends grpc.MethodDefinition<itbridge_pb.PayloadEmpty, itbridge_pb.TracksList> {
    path: string; // "/itbridge.iTBridgeService/TracksGetSelected"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<itbridge_pb.PayloadEmpty>;
    requestDeserialize: grpc.deserialize<itbridge_pb.PayloadEmpty>;
    responseSerialize: grpc.serialize<itbridge_pb.TracksList>;
    responseDeserialize: grpc.deserialize<itbridge_pb.TracksList>;
}
interface IiTBridgeServiceService_ITrackSetLyrics extends grpc.MethodDefinition<itbridge_pb.TrackIdAndLyrics, itbridge_pb.PayloadOperationResult> {
    path: string; // "/itbridge.iTBridgeService/TrackSetLyrics"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<itbridge_pb.TrackIdAndLyrics>;
    requestDeserialize: grpc.deserialize<itbridge_pb.TrackIdAndLyrics>;
    responseSerialize: grpc.serialize<itbridge_pb.PayloadOperationResult>;
    responseDeserialize: grpc.deserialize<itbridge_pb.PayloadOperationResult>;
}
interface IiTBridgeServiceService_ITrackGetHasLyrics extends grpc.MethodDefinition<itbridge_pb.iTObjectId, itbridge_pb.PayloadBoolean> {
    path: string; // "/itbridge.iTBridgeService/TrackGetHasLyrics"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<itbridge_pb.iTObjectId>;
    requestDeserialize: grpc.deserialize<itbridge_pb.iTObjectId>;
    responseSerialize: grpc.serialize<itbridge_pb.PayloadBoolean>;
    responseDeserialize: grpc.deserialize<itbridge_pb.PayloadBoolean>;
}
interface IiTBridgeServiceService_IPlaylistsGet extends grpc.MethodDefinition<itbridge_pb.PayloadEmpty, itbridge_pb.PlaylistsList> {
    path: string; // "/itbridge.iTBridgeService/PlaylistsGet"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<itbridge_pb.PayloadEmpty>;
    requestDeserialize: grpc.deserialize<itbridge_pb.PayloadEmpty>;
    responseSerialize: grpc.serialize<itbridge_pb.PlaylistsList>;
    responseDeserialize: grpc.deserialize<itbridge_pb.PlaylistsList>;
}
interface IiTBridgeServiceService_IPlaylistGetTracks extends grpc.MethodDefinition<itbridge_pb.iTObjectId, itbridge_pb.TracksList> {
    path: string; // "/itbridge.iTBridgeService/PlaylistGetTracks"
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<itbridge_pb.iTObjectId>;
    requestDeserialize: grpc.deserialize<itbridge_pb.iTObjectId>;
    responseSerialize: grpc.serialize<itbridge_pb.TracksList>;
    responseDeserialize: grpc.deserialize<itbridge_pb.TracksList>;
}

export const iTBridgeServiceService: IiTBridgeServiceService;

export interface IiTBridgeServiceServer {
    hello: grpc.handleUnaryCall<itbridge_pb.PayloadEmpty, itbridge_pb.HelloReply>;
    tracksStreamSelected: grpc.handleServerStreamingCall<itbridge_pb.PayloadEmpty, itbridge_pb.TracksListStream>;
    tracksGetSelected: grpc.handleUnaryCall<itbridge_pb.PayloadEmpty, itbridge_pb.TracksList>;
    trackSetLyrics: grpc.handleUnaryCall<itbridge_pb.TrackIdAndLyrics, itbridge_pb.PayloadOperationResult>;
    trackGetHasLyrics: grpc.handleUnaryCall<itbridge_pb.iTObjectId, itbridge_pb.PayloadBoolean>;
    playlistsGet: grpc.handleUnaryCall<itbridge_pb.PayloadEmpty, itbridge_pb.PlaylistsList>;
    playlistGetTracks: grpc.handleUnaryCall<itbridge_pb.iTObjectId, itbridge_pb.TracksList>;
}

export interface IiTBridgeServiceClient {
    hello(request: itbridge_pb.PayloadEmpty, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    hello(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    hello(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    tracksStreamSelected(request: itbridge_pb.PayloadEmpty, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<itbridge_pb.TracksListStream>;
    tracksStreamSelected(request: itbridge_pb.PayloadEmpty, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<itbridge_pb.TracksListStream>;
    tracksGetSelected(request: itbridge_pb.PayloadEmpty, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
    tracksGetSelected(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
    tracksGetSelected(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
    trackSetLyrics(request: itbridge_pb.TrackIdAndLyrics, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadOperationResult) => void): grpc.ClientUnaryCall;
    trackSetLyrics(request: itbridge_pb.TrackIdAndLyrics, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadOperationResult) => void): grpc.ClientUnaryCall;
    trackSetLyrics(request: itbridge_pb.TrackIdAndLyrics, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadOperationResult) => void): grpc.ClientUnaryCall;
    trackGetHasLyrics(request: itbridge_pb.iTObjectId, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadBoolean) => void): grpc.ClientUnaryCall;
    trackGetHasLyrics(request: itbridge_pb.iTObjectId, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadBoolean) => void): grpc.ClientUnaryCall;
    trackGetHasLyrics(request: itbridge_pb.iTObjectId, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadBoolean) => void): grpc.ClientUnaryCall;
    playlistsGet(request: itbridge_pb.PayloadEmpty, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PlaylistsList) => void): grpc.ClientUnaryCall;
    playlistsGet(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PlaylistsList) => void): grpc.ClientUnaryCall;
    playlistsGet(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PlaylistsList) => void): grpc.ClientUnaryCall;
    playlistGetTracks(request: itbridge_pb.iTObjectId, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
    playlistGetTracks(request: itbridge_pb.iTObjectId, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
    playlistGetTracks(request: itbridge_pb.iTObjectId, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
}

export class iTBridgeServiceClient extends grpc.Client implements IiTBridgeServiceClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public hello(request: itbridge_pb.PayloadEmpty, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    public hello(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    public hello(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.HelloReply) => void): grpc.ClientUnaryCall;
    public tracksStreamSelected(request: itbridge_pb.PayloadEmpty, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<itbridge_pb.TracksListStream>;
    public tracksStreamSelected(request: itbridge_pb.PayloadEmpty, metadata?: grpc.Metadata, options?: Partial<grpc.CallOptions>): grpc.ClientReadableStream<itbridge_pb.TracksListStream>;
    public tracksGetSelected(request: itbridge_pb.PayloadEmpty, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
    public tracksGetSelected(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
    public tracksGetSelected(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
    public trackSetLyrics(request: itbridge_pb.TrackIdAndLyrics, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadOperationResult) => void): grpc.ClientUnaryCall;
    public trackSetLyrics(request: itbridge_pb.TrackIdAndLyrics, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadOperationResult) => void): grpc.ClientUnaryCall;
    public trackSetLyrics(request: itbridge_pb.TrackIdAndLyrics, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadOperationResult) => void): grpc.ClientUnaryCall;
    public trackGetHasLyrics(request: itbridge_pb.iTObjectId, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadBoolean) => void): grpc.ClientUnaryCall;
    public trackGetHasLyrics(request: itbridge_pb.iTObjectId, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadBoolean) => void): grpc.ClientUnaryCall;
    public trackGetHasLyrics(request: itbridge_pb.iTObjectId, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PayloadBoolean) => void): grpc.ClientUnaryCall;
    public playlistsGet(request: itbridge_pb.PayloadEmpty, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PlaylistsList) => void): grpc.ClientUnaryCall;
    public playlistsGet(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PlaylistsList) => void): grpc.ClientUnaryCall;
    public playlistsGet(request: itbridge_pb.PayloadEmpty, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.PlaylistsList) => void): grpc.ClientUnaryCall;
    public playlistGetTracks(request: itbridge_pb.iTObjectId, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
    public playlistGetTracks(request: itbridge_pb.iTObjectId, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
    public playlistGetTracks(request: itbridge_pb.iTObjectId, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: itbridge_pb.TracksList) => void): grpc.ClientUnaryCall;
}
