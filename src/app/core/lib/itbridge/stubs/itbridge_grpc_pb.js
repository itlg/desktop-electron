// GENERATED CODE -- DO NOT EDIT!

'use strict';
var itbridge_pb = require('./itbridge_pb.js');

function serialize_itbridge_HelloReply(arg) {
  if (!(arg instanceof itbridge_pb.HelloReply)) {
    throw new Error('Expected argument of type itbridge.HelloReply');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_HelloReply(buffer_arg) {
  return itbridge_pb.HelloReply.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_PayloadBoolean(arg) {
  if (!(arg instanceof itbridge_pb.PayloadBoolean)) {
    throw new Error('Expected argument of type itbridge.PayloadBoolean');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_PayloadBoolean(buffer_arg) {
  return itbridge_pb.PayloadBoolean.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_PayloadEmpty(arg) {
  if (!(arg instanceof itbridge_pb.PayloadEmpty)) {
    throw new Error('Expected argument of type itbridge.PayloadEmpty');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_PayloadEmpty(buffer_arg) {
  return itbridge_pb.PayloadEmpty.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_PayloadOperationResult(arg) {
  if (!(arg instanceof itbridge_pb.PayloadOperationResult)) {
    throw new Error('Expected argument of type itbridge.PayloadOperationResult');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_PayloadOperationResult(buffer_arg) {
  return itbridge_pb.PayloadOperationResult.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_PlaylistsList(arg) {
  if (!(arg instanceof itbridge_pb.PlaylistsList)) {
    throw new Error('Expected argument of type itbridge.PlaylistsList');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_PlaylistsList(buffer_arg) {
  return itbridge_pb.PlaylistsList.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_TrackIdAndLyrics(arg) {
  if (!(arg instanceof itbridge_pb.TrackIdAndLyrics)) {
    throw new Error('Expected argument of type itbridge.TrackIdAndLyrics');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_TrackIdAndLyrics(buffer_arg) {
  return itbridge_pb.TrackIdAndLyrics.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_TracksList(arg) {
  if (!(arg instanceof itbridge_pb.TracksList)) {
    throw new Error('Expected argument of type itbridge.TracksList');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_TracksList(buffer_arg) {
  return itbridge_pb.TracksList.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_TracksListStream(arg) {
  if (!(arg instanceof itbridge_pb.TracksListStream)) {
    throw new Error('Expected argument of type itbridge.TracksListStream');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_TracksListStream(buffer_arg) {
  return itbridge_pb.TracksListStream.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_itbridge_iTObjectId(arg) {
  if (!(arg instanceof itbridge_pb.iTObjectId)) {
    throw new Error('Expected argument of type itbridge.iTObjectId');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_itbridge_iTObjectId(buffer_arg) {
  return itbridge_pb.iTObjectId.deserializeBinary(new Uint8Array(buffer_arg));
}


var iTBridgeServiceService = exports['itbridge.iTBridgeService'] = {
  hello: {
    path: '/itbridge.iTBridgeService/Hello',
    requestStream: false,
    responseStream: false,
    requestType: itbridge_pb.PayloadEmpty,
    responseType: itbridge_pb.HelloReply,
    requestSerialize: serialize_itbridge_PayloadEmpty,
    requestDeserialize: deserialize_itbridge_PayloadEmpty,
    responseSerialize: serialize_itbridge_HelloReply,
    responseDeserialize: deserialize_itbridge_HelloReply,
  },
  tracksStreamSelected: {
    path: '/itbridge.iTBridgeService/TracksStreamSelected',
    requestStream: false,
    responseStream: true,
    requestType: itbridge_pb.PayloadEmpty,
    responseType: itbridge_pb.TracksListStream,
    requestSerialize: serialize_itbridge_PayloadEmpty,
    requestDeserialize: deserialize_itbridge_PayloadEmpty,
    responseSerialize: serialize_itbridge_TracksListStream,
    responseDeserialize: deserialize_itbridge_TracksListStream,
  },
  tracksGetSelected: {
    path: '/itbridge.iTBridgeService/TracksGetSelected',
    requestStream: false,
    responseStream: false,
    requestType: itbridge_pb.PayloadEmpty,
    responseType: itbridge_pb.TracksList,
    requestSerialize: serialize_itbridge_PayloadEmpty,
    requestDeserialize: deserialize_itbridge_PayloadEmpty,
    responseSerialize: serialize_itbridge_TracksList,
    responseDeserialize: deserialize_itbridge_TracksList,
  },
  trackSetLyrics: {
    path: '/itbridge.iTBridgeService/TrackSetLyrics',
    requestStream: false,
    responseStream: false,
    requestType: itbridge_pb.TrackIdAndLyrics,
    responseType: itbridge_pb.PayloadOperationResult,
    requestSerialize: serialize_itbridge_TrackIdAndLyrics,
    requestDeserialize: deserialize_itbridge_TrackIdAndLyrics,
    responseSerialize: serialize_itbridge_PayloadOperationResult,
    responseDeserialize: deserialize_itbridge_PayloadOperationResult,
  },
  trackGetHasLyrics: {
    path: '/itbridge.iTBridgeService/TrackGetHasLyrics',
    requestStream: false,
    responseStream: false,
    requestType: itbridge_pb.iTObjectId,
    responseType: itbridge_pb.PayloadBoolean,
    requestSerialize: serialize_itbridge_iTObjectId,
    requestDeserialize: deserialize_itbridge_iTObjectId,
    responseSerialize: serialize_itbridge_PayloadBoolean,
    responseDeserialize: deserialize_itbridge_PayloadBoolean,
  },
  playlistsGet: {
    path: '/itbridge.iTBridgeService/PlaylistsGet',
    requestStream: false,
    responseStream: false,
    requestType: itbridge_pb.PayloadEmpty,
    responseType: itbridge_pb.PlaylistsList,
    requestSerialize: serialize_itbridge_PayloadEmpty,
    requestDeserialize: deserialize_itbridge_PayloadEmpty,
    responseSerialize: serialize_itbridge_PlaylistsList,
    responseDeserialize: deserialize_itbridge_PlaylistsList,
  },
  playlistGetTracks: {
    path: '/itbridge.iTBridgeService/PlaylistGetTracks',
    requestStream: false,
    responseStream: false,
    requestType: itbridge_pb.iTObjectId,
    responseType: itbridge_pb.TracksList,
    requestSerialize: serialize_itbridge_iTObjectId,
    requestDeserialize: deserialize_itbridge_iTObjectId,
    responseSerialize: serialize_itbridge_TracksList,
    responseDeserialize: deserialize_itbridge_TracksList,
  },
};

