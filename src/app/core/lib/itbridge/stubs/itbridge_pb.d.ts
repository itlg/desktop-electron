// package: itbridge
// file: itbridge.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class PayloadEmpty extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PayloadEmpty.AsObject;
    static toObject(includeInstance: boolean, msg: PayloadEmpty): PayloadEmpty.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PayloadEmpty, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PayloadEmpty;
    static deserializeBinaryFromReader(message: PayloadEmpty, reader: jspb.BinaryReader): PayloadEmpty;
}

export namespace PayloadEmpty {
    export type AsObject = {
    }
}

export class PayloadOperationResult extends jspb.Message { 
    getOk(): boolean;
    setOk(value: boolean): PayloadOperationResult;

    getResultId(): number;
    setResultId(value: number): PayloadOperationResult;

    getMessage(): string;
    setMessage(value: string): PayloadOperationResult;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PayloadOperationResult.AsObject;
    static toObject(includeInstance: boolean, msg: PayloadOperationResult): PayloadOperationResult.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PayloadOperationResult, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PayloadOperationResult;
    static deserializeBinaryFromReader(message: PayloadOperationResult, reader: jspb.BinaryReader): PayloadOperationResult;
}

export namespace PayloadOperationResult {
    export type AsObject = {
        ok: boolean,
        resultId: number,
        message: string,
    }
}

export class PayloadBoolean extends jspb.Message { 
    getContent(): boolean;
    setContent(value: boolean): PayloadBoolean;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PayloadBoolean.AsObject;
    static toObject(includeInstance: boolean, msg: PayloadBoolean): PayloadBoolean.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PayloadBoolean, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PayloadBoolean;
    static deserializeBinaryFromReader(message: PayloadBoolean, reader: jspb.BinaryReader): PayloadBoolean;
}

export namespace PayloadBoolean {
    export type AsObject = {
        content: boolean,
    }
}

export class PayloadString extends jspb.Message { 
    getContent(): string;
    setContent(value: string): PayloadString;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PayloadString.AsObject;
    static toObject(includeInstance: boolean, msg: PayloadString): PayloadString.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PayloadString, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PayloadString;
    static deserializeBinaryFromReader(message: PayloadString, reader: jspb.BinaryReader): PayloadString;
}

export namespace PayloadString {
    export type AsObject = {
        content: string,
    }
}

export class iTObjectId extends jspb.Message { 
    getIdSource(): number;
    setIdSource(value: number): iTObjectId;

    getIdPlaylist(): number;
    setIdPlaylist(value: number): iTObjectId;

    getIdTrack(): number;
    setIdTrack(value: number): iTObjectId;

    getIdDatabase(): number;
    setIdDatabase(value: number): iTObjectId;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): iTObjectId.AsObject;
    static toObject(includeInstance: boolean, msg: iTObjectId): iTObjectId.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: iTObjectId, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): iTObjectId;
    static deserializeBinaryFromReader(message: iTObjectId, reader: jspb.BinaryReader): iTObjectId;
}

export namespace iTObjectId {
    export type AsObject = {
        idSource: number,
        idPlaylist: number,
        idTrack: number,
        idDatabase: number,
    }
}

export class Track extends jspb.Message { 

    hasId(): boolean;
    clearId(): void;
    getId(): iTObjectId | undefined;
    setId(value?: iTObjectId): Track;

    getTitle(): string;
    setTitle(value: string): Track;

    getArtist(): string;
    setArtist(value: string): Track;

    getAlbum(): string;
    setAlbum(value: string): Track;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Track.AsObject;
    static toObject(includeInstance: boolean, msg: Track): Track.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Track, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Track;
    static deserializeBinaryFromReader(message: Track, reader: jspb.BinaryReader): Track;
}

export namespace Track {
    export type AsObject = {
        id?: iTObjectId.AsObject,
        title: string,
        artist: string,
        album: string,
    }
}

export class TracksList extends jspb.Message { 
    getQuantity(): number;
    setQuantity(value: number): TracksList;

    clearTracksList(): void;
    getTracksList(): Array<Track>;
    setTracksList(value: Array<Track>): TracksList;
    addTracks(value?: Track, index?: number): Track;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TracksList.AsObject;
    static toObject(includeInstance: boolean, msg: TracksList): TracksList.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TracksList, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TracksList;
    static deserializeBinaryFromReader(message: TracksList, reader: jspb.BinaryReader): TracksList;
}

export namespace TracksList {
    export type AsObject = {
        quantity: number,
        tracksList: Array<Track.AsObject>,
    }
}

export class TracksListStream extends jspb.Message { 
    getCurrent(): number;
    setCurrent(value: number): TracksListStream;

    getTotal(): number;
    setTotal(value: number): TracksListStream;


    hasTrack(): boolean;
    clearTrack(): void;
    getTrack(): Track | undefined;
    setTrack(value?: Track): TracksListStream;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TracksListStream.AsObject;
    static toObject(includeInstance: boolean, msg: TracksListStream): TracksListStream.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TracksListStream, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TracksListStream;
    static deserializeBinaryFromReader(message: TracksListStream, reader: jspb.BinaryReader): TracksListStream;
}

export namespace TracksListStream {
    export type AsObject = {
        current: number,
        total: number,
        track?: Track.AsObject,
    }
}

export class TrackIdAndLyrics extends jspb.Message { 

    hasId(): boolean;
    clearId(): void;
    getId(): iTObjectId | undefined;
    setId(value?: iTObjectId): TrackIdAndLyrics;

    getLyrics(): string;
    setLyrics(value: string): TrackIdAndLyrics;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TrackIdAndLyrics.AsObject;
    static toObject(includeInstance: boolean, msg: TrackIdAndLyrics): TrackIdAndLyrics.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TrackIdAndLyrics, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TrackIdAndLyrics;
    static deserializeBinaryFromReader(message: TrackIdAndLyrics, reader: jspb.BinaryReader): TrackIdAndLyrics;
}

export namespace TrackIdAndLyrics {
    export type AsObject = {
        id?: iTObjectId.AsObject,
        lyrics: string,
    }
}

export class Playlist extends jspb.Message { 

    hasId(): boolean;
    clearId(): void;
    getId(): iTObjectId | undefined;
    setId(value?: iTObjectId): Playlist;

    getName(): string;
    setName(value: string): Playlist;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Playlist.AsObject;
    static toObject(includeInstance: boolean, msg: Playlist): Playlist.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Playlist, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Playlist;
    static deserializeBinaryFromReader(message: Playlist, reader: jspb.BinaryReader): Playlist;
}

export namespace Playlist {
    export type AsObject = {
        id?: iTObjectId.AsObject,
        name: string,
    }
}

export class PlaylistsList extends jspb.Message { 
    getQuantity(): number;
    setQuantity(value: number): PlaylistsList;

    clearPlaylistsList(): void;
    getPlaylistsList(): Array<Playlist>;
    setPlaylistsList(value: Array<Playlist>): PlaylistsList;
    addPlaylists(value?: Playlist, index?: number): Playlist;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): PlaylistsList.AsObject;
    static toObject(includeInstance: boolean, msg: PlaylistsList): PlaylistsList.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: PlaylistsList, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): PlaylistsList;
    static deserializeBinaryFromReader(message: PlaylistsList, reader: jspb.BinaryReader): PlaylistsList;
}

export namespace PlaylistsList {
    export type AsObject = {
        quantity: number,
        playlistsList: Array<Playlist.AsObject>,
    }
}

export class HelloReply extends jspb.Message { 
    getAppName(): string;
    setAppName(value: string): HelloReply;

    getAppVersion(): string;
    setAppVersion(value: string): HelloReply;


    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): HelloReply.AsObject;
    static toObject(includeInstance: boolean, msg: HelloReply): HelloReply.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: HelloReply, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): HelloReply;
    static deserializeBinaryFromReader(message: HelloReply, reader: jspb.BinaryReader): HelloReply;
}

export namespace HelloReply {
    export type AsObject = {
        appName: string,
        appVersion: string,
    }
}
