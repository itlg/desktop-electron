import {Injectable} from '@angular/core';
import {ElectronService} from "../electron/electron.service";
import {
  IPCElectronStore,
  IPCElectronStoreActions,
  IPCElectronStoreKeys
} from "../../ipc/IPCElectronStore";

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  constructor(public electronService: ElectronService) {
  }

  async get(key: IPCElectronStoreKeys) {
    return await this.electronService.ipcRenderer.invoke(
      IPCElectronStore.getMainActionId(IPCElectronStoreActions.GET_VALUE),
      IPCElectronStore.storeKeyToString(key)
    )
  }

  async set(key: IPCElectronStoreKeys, value: any) {
    return await this.electronService.ipcRenderer.invoke(
      IPCElectronStore.getMainActionId(IPCElectronStoreActions.SET_VALUE),
      IPCElectronStore.storeKeyToString(key),
      value
    )
  }
}
