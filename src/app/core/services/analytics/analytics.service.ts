import {Injectable} from '@angular/core';
import {StoreService} from "../store/store.service";
import GAnalytics from "../../lib/ganalytics/gAnalytics";
import {IPCElectronStoreKeys} from "../../ipc/IPCElectronStore";
import {v4 as uuidv4} from 'uuid';
import logger from "../../utils/log";
import {TrackStatus} from "../../../home/lyrics-grabber/processedTracks.datasource";

const ACTION_CATEGORY_USER = "user";
const ACTION_CATEGORY_APP = "app";

const ACTION_STARTED_APP_NAME = "started-app";
const ACTION_PRESSED_NAME = "button-pressed";
const ACTION_PRESSED_GRAB_LABEL = "grab";
const ACTION_DOWNLOAD_LYRICS_NAME = "download-lyrics";
const ACTION_DOWNLOAD_LYRICS_STARTED_LABEL = "started";
const ACTION_DOWNLOAD_LYRICS_SUCCESS_LABEL = "success";
const ACTION_DOWNLOAD_LYRICS_ERROR_LABEL = "error";

const EXCEPTION_PREFIX = "EX";

const MODULE = "AnalyticsService"

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  private mGAnalytics = undefined
  private mAnalyticsEnabled = undefined

  constructor(public store: StoreService) {
    // request new uuid if not one
    store.get(IPCElectronStoreKeys.USER_ANONYMOUS_ID).then(uuid => {
      logger.debug(`constructor: retrieved UUID is ${uuid}`, {module: MODULE})
      let uuidToUse = uuid
      if (uuid == undefined || uuid == "" || typeof uuid != "string") {
        uuidToUse = uuidv4()
        store.set(IPCElectronStoreKeys.USER_ANONYMOUS_ID, uuidToUse).then(res => {
          logger.debug("constructor: new uuid saved", {module: MODULE})
        })
      }

      // create GAnalytics instance
      this.mGAnalytics = new GAnalytics("UA-138506373-1", uuidToUse)
    })

    store.get(IPCElectronStoreKeys.CONFIGURATION_ANALYTICS).then(res => {
      this.mAnalyticsEnabled = res
    })
  }

  private getGAnalyticsObject() {
    return new Promise<Boolean>((resolve, reject) => {
      let checkValue = () => {
        if (this.mGAnalytics != undefined) {
          resolve(true)
          return true
        } else {
          return false
        }
      }

      if (!checkValue()) setTimeout(checkValue, 500)
    })
  }

  async logStartedApplication(appVersion: string) {
    this.getGAnalyticsObject().then(_ => {
      this.mGAnalytics.logEvent(ACTION_CATEGORY_USER, ACTION_STARTED_APP_NAME, appVersion, 0)
    })
  }

  logGrabPressed() {
    this.getGAnalyticsObject().then(_ => {
      this.mGAnalytics.logEvent(ACTION_CATEGORY_USER, ACTION_PRESSED_NAME, ACTION_PRESSED_GRAB_LABEL, 0)
    })
  }

  logDownloadLyricsStarted() {
    this.getGAnalyticsObject().then(_ => {
      this.mGAnalytics.logEvent(ACTION_CATEGORY_APP, ACTION_DOWNLOAD_LYRICS_NAME, ACTION_DOWNLOAD_LYRICS_STARTED_LABEL, 0)
    })
  }

  logDownloadLyricsError() {
    this.getGAnalyticsObject().then(_ => {
      this.mGAnalytics.logEvent(ACTION_CATEGORY_APP, ACTION_DOWNLOAD_LYRICS_NAME, ACTION_DOWNLOAD_LYRICS_ERROR_LABEL, 0)
    })
  }

  logDownloadLyricsSuccess() {
    this.getGAnalyticsObject().then(_ => {
      this.mGAnalytics.logEvent(ACTION_CATEGORY_APP, ACTION_DOWNLOAD_LYRICS_NAME, ACTION_DOWNLOAD_LYRICS_SUCCESS_LABEL, 0)
    })
  }

  logDownloadLyricsEndStatus(s: TrackStatus) {
    this.getGAnalyticsObject().then(_ => {
      this.mGAnalytics.logEvent(ACTION_CATEGORY_APP, ACTION_DOWNLOAD_LYRICS_NAME, s, 0)
    })
  }

  logException(code: Number, fatal: Boolean) {
    this.getGAnalyticsObject().then(_ => {
      this.mGAnalytics.logException(`${EXCEPTION_PREFIX} + ${code}`, fatal)
    })
  }
}
