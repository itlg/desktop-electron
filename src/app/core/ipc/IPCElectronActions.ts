import {IPCElectronStoreActions} from "./IPCElectronStore";

export enum IPCElectronActionsActions {
  OPEN_EXTERNAL_URL= "OPEN_EXTERNAL_URL"
}

export enum IPCElectronActionsKeys {
  URL
}


export class IPCElectronActions {

  public static getMainActionId(action: IPCElectronActionsActions) {
    switch (action) {
      case IPCElectronActionsActions.OPEN_EXTERNAL_URL:
        return "getStoreValue"
    }
  }
}

