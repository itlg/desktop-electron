export enum IPCElectronStoreActions {
  SET_VALUE, GET_VALUE
}

export enum IPCElectronStoreKeys {
  CONFIGURATION,
  CONFIGURATION_OVERWRITE_LYRICS,
  CONFIGURATION_LOGGING,
  CONFIGURATION_ANALYTICS,
  CONFIGURATION_AUTO_CHECK_UPDATES,
  USER_ANONYMOUS_ID
}


export class IPCElectronStore {

  public static getMainActionId(action: IPCElectronStoreActions) {
    switch (action) {
      case IPCElectronStoreActions.GET_VALUE:
        return "getStoreValue"
      case IPCElectronStoreActions.SET_VALUE:
        return "setStoreValue"
    }
  }

  public static storeKeyToString(key: IPCElectronStoreKeys) {
    switch (key) {
      case IPCElectronStoreKeys.CONFIGURATION:
        return "configuration"
      case IPCElectronStoreKeys.CONFIGURATION_ANALYTICS:
        return "configuration.analytics"
      case IPCElectronStoreKeys.CONFIGURATION_LOGGING:
        return "configuration.debugLogging"
      case IPCElectronStoreKeys.CONFIGURATION_OVERWRITE_LYRICS:
        return "configuration.overwriteLyrics"
      case IPCElectronStoreKeys.CONFIGURATION_AUTO_CHECK_UPDATES:
        return "configuration.autoCheckUpdates"
      case IPCElectronStoreKeys.USER_ANONYMOUS_ID:
        return "user.anonymousId"
    }
  }

  public static getConfigurationDefaultValue(key: string) {
    switch (key) {
      case this.storeKeyToString(IPCElectronStoreKeys.CONFIGURATION_ANALYTICS):
        return true
      case this.storeKeyToString(IPCElectronStoreKeys.CONFIGURATION_LOGGING):
        return false
      case this.storeKeyToString(IPCElectronStoreKeys.CONFIGURATION_OVERWRITE_LYRICS):
        return false
      case this.storeKeyToString(IPCElectronStoreKeys.CONFIGURATION_AUTO_CHECK_UPDATES):
        return true
      default:
        return undefined
    }
  }

  public static getUserDefaultValue(key: string) {
    switch (key) {
      case this.storeKeyToString(IPCElectronStoreKeys.USER_ANONYMOUS_ID):
        return ""
      default:
        return undefined
    }
  }

}
