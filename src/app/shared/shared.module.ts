import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { PageNotFoundComponent } from './components/';
import { WebviewDirective } from './directives/';
import { FormsModule } from '@angular/forms';
import { StatusDotComponent } from './components/status-dot/status-dot.component';
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  declarations: [PageNotFoundComponent, WebviewDirective, StatusDotComponent],
  imports: [CommonModule, TranslateModule, FormsModule, MatIconModule],
    exports: [TranslateModule, WebviewDirective, FormsModule, StatusDotComponent]
})
export class SharedModule {}
