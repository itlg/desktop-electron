import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-status-dot',
  templateUrl: './status-dot.component.html',
  styleUrls: ['./status-dot.component.css']
})
export class StatusDotComponent implements OnInit {
  private _init = false
  private _size = 10

  @Input()
  public color: string

  @Input()
  public blinking: boolean

  @Input()
  set size(n: number) {
    if(this._init) return
    this._init = true
    this._size =n
  }

  get size() { return this._size  }

  constructor() {
  }

  ngOnInit(): void {
  }

}
